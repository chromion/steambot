const SteamUser = require('steam-user');
let request = require('request');
const Cheerio = require('cheerio');
const program = require('commander');
const config = require('./config.json')

// load account config
const logOnOptions = config.account;

const botOptions = config.bot;

// Controllers
const IdleController = require('./app/Controllers/IdleController.js');

const client = new SteamUser({
  enablePicsCache: true,
});

client.logOn(logOnOptions);

client.on('loggedOn', () => {
  console.log('Logged into Steam');
  client.setPersona(botOptions.status);
});

program
.version('1.0')
.description('SteamBot');

program
.command('idle')
.description('idle games to remain card drops')
.action(() => {
  client.on('appOwnershipCached', () => {
    IdleController.idle(client);
  });
});

program.parse(process.argv);

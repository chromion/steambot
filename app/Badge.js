let request = require('request');
const Cheerio = require('cheerio');

const g_Jar = request.jar();
request = request.defaults({'jar': g_Jar});

class Badge {

  /**
   * Returns all badges that have drops left
   * @param  {SteamUser}  client node_modules/steam-SteamUser
   * @return {Array}
   */
  static allDrops(client) {
    let _games = [];
    return new Promise((resolve, reject) => {
      client.webLogOn();
      client.once('webSession', (sessionID, cookies) => {
        cookies.forEach((cookie) => {
          g_Jar.setCookie(cookie, 'https://steamcommunity.com');
        });

        request('https://steamcommunity.com/my/badges/?l=english', (err, response, body) => {

          var ownedPackages = client.licenses.map((license) => {
            var _package = client.picsCache.packages[license.package_id].packageinfo;
            _package.time_created = license.time_created;
            _package.payment_method = license.payment_method;

            return _package;
          }).filter((_package) => {
            return !(_package.extended && _package.extended.freeweekend);
          });

          const $_ = Cheerio.load(body);

          $_('.badge_row').each(function(badgeIndex) {
            const row = $_(this);
            const overlay = row.find('.badge_row_overlay');
            if(!overlay) {
              return;
            }

            const match = overlay.attr('href').match(/\/gamecards\/(\d+)/);
            if(!match) {
              return;
            }

            const appid = parseInt(match[1], 10);

            let name = row.find('.badge_title');
            name.find('.badge_view_details').remove();
            name = name.text().replace(/\n/g, '').replace(/\r/g, '').replace(/\t/g, '').trim();

            if(!client.picsCache.apps.hasOwnProperty(appid)) {
              return;
            }

            let newlyPurchased = false;
            ownedPackages.filter((_package) => {
              return _package.appids && _package.appids.indexOf(appid) != -1;
            }).forEach((_package) => {
              const timeCreatedAgo = Math.floor(Date.now() / 1000) - _package.time_created;
              if(timeCreatedAgo < (60 * 60 * 24 * 14) && [Steam.EPaymentMethod.ActivationCode, Steam.EPaymentMethod.GuestPass, Steam.EPaymentMethod.Complimentary].indexOf(_package.payment_method) == -1) {
                newlyPurchased = true;
              }
            });

            let drops = row.find('.progress_info_bold').text().match(/(\d+) card drops? remaining/);
            if(!drops) {
              return;
            }

            drops = parseInt(drops[1], 10);
            if(isNaN(drops) || drops < 1) {
              return;
            }

            let playtime = row.find('.badge_title_stats_playtime').text().trim().match(/(\d+\.\d+).hrs on record/);
            if(!playtime) {
              playtime = 0.0;
            } else {
              playtime = parseFloat(playtime[1], 10);
              if(isNaN(playtime)) {
                playtime = 0.0;
              }
            }

            _games.push({appid, name, drops, playtime});

          });

          resolve(_games);
        });
      });
    });
  }
}

module.exports = Badge;

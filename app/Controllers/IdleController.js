const config = require('./../../config.json').bot;
const Badge = require('./../Badge.js');

class IdleController {

  /**
   * Idle games to receive card drops
   * @param  {SteamUser} client node_modules/steam-user
   */
  static idle(client) {
    var interval;

    // Intern function to repeat the idle process
    function _idle() {
      // get Badges with remaining drops
      Badge.allDrops(client).then(games => {
        if(games.length > 0) {
          console.log(`start idle the game ${games[0].name} for ${config.idleTime}, ${games[0].drops} remaining}`);
          // TODO: implement kind of sort: fastest, ascending or decending order
          // idle the first game in the list
          client.gamesPlayed(games[0]['appid']);
          // idle the game for "config.idleTime" minutes
          interval = setInterval(() => {
            console.log(`restart the game ${games[0].name} to idle the next ${config.idleTime}`);
            // If you stop the game new cards appear
            client.gamesPlayed([]);
            // Restart the game to continue idle
            client.gamesPlayed(games[0]['appid']);
          }, 1000 * 60 * config.idleTime);
        }
        // no badges with drops remaining
        else {
          console.log('no more games with remaining drops to idle');
          // TODO: logout from steam
          // process.exit(0);
        }
      });
    }

    _idle();

    // Check for new items in the inventory
    client.on('newItems', (count) => {
      console.log(`${count} new item(s) in your Inventory`);
      // stop the interval that idle games every "config.idleTime" minutes
      clearInterval(interval);
      // stop idle the current game
      client.gamesPlayed([]);
      // TODO: check if game has still card drops left and games array length bigger than 1
      // continue idle
      _idle();
    });
  }

}

module.exports = IdleController;

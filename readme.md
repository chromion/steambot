# SteamBot

### A bot to farm steam cards

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)](https://gitlab.com/chromion/steambot/blob/master/LICENSE)

The node modules "steam-user" and "steamcommunity" reports anonymous usage statistics to the author. See [here](https://github.com/DoctorMcKay/node-stats-reporter) for more informations.

### Warning

The usage of this bot will waive your rights to a refund. See [here](http://store.steampowered.com/steam_refunds/) for more informations.

---

#### Getting started
1. Download or clone the repository
2. Execute follwing command
```
npm install --save
```
3. Rename the *config.json.exmaple* into *config.json*
4. Enter your steam username and password in the config.json
